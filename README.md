## Welcome to the Big Book of Julia!

The idea for Big Book of Julia (BBJ) is based on [Big Book of R](https://www.bigbookofr.com/index.html).

Its objective is to be a set of resources for [Julia](https://julialang.org/) programmers, data scientists and researchers. It contains links to tutorials, courses, blogs, videos, package documentation and books related to Julia programming.

This content is free to use and is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License](https://creativecommons.org/licenses/by-nc-nd/3.0/us/).

---

## Contributors

Created and maintained by [Adam Wysokiński](mailto:adam.wysokinski@umed.lodz.pl).

If you've contributed, add your name below:

Adam Wysokiński, jluis, uncomfyhalomacro.

---

\toc

## Language documentation

[Julia Documentation](https://docs.julialang.org/en/v1/)

This is the official Julia documentation.

---

## Videos

[The Julia Programming Language](https://www.youtube.com/c/TheJuliaLanguage/videos)

Alan Edelman, Viral B. Shah [Julia Lightning Round](https://www.youtube.com/watch?v=37L1OMk_3FU)

Keno Fischer [Networking in Julia](https://www.youtube.com/watch?v=qYjHYTn7r2w)

Jeff Bezanson [Parallel and Distributed Computing with Julia](https://www.youtube.com/watch?v=JoRn4ryMclc)

Stefan Karpinski [Metaprogramming and Macros in Julia](https://www.youtube.com/watch?v=EpNeNCGmyZE)

Miles Lubin, Iain Dunning [Numerical Optimization in Julia](https://www.youtube.com/watch?v=O1icUP6sajU)

Steve Johnson [Fast Fourier Transforms in Julia](https://www.youtube.com/watch?v=1iBLaHGL1AM)

Doug Bates [Statistical Models in Julia](https://www.youtube.com/watch?v=v9Io-p_iymI)

John Myles White [Data Analysis in Julia with Data Frames](https://www.youtube.com/watch?v=XRClA5YLiIc)

Jeff Bezanson, Stefan Karpinski [Rationale Behind Julia and the Vision](https://www.youtube.com/watch?v=02U9AJMEWx0)

[Julia for Talented Amateurs](https://www.youtube.com/c/juliafortalentedamateurs/videos)

Alan Edelman [Matrix Methods in Data Analysis, Signal Processing, and Machine Learning](https://www.youtube.com/watch?v=rZS2LGiurKY)

Alan Edelman [A programming language to heal the planet together: Julia](https://www.youtube.com/watch?v=qGW0GT1rCvs)

---

## Julia Academy Courses

[JuliaAcademy](https://juliaacademy.com/courses)

---

## Blogs / Forums

[Julia Computing](https://juliacomputing.com/blog/)

[Julia Forem](https://forem.julialang.org/)

[Discourse](https://discourse.julialang.org/)

[Zulip](https://julialang.zulipchat.com)

[Discord](https://humansofjulia.org/)

---

## Packages

[Pkg.jl](https://pkgdocs.julialang.org/v1/)

Documentation for Pkg, Julia's package manager.

[PkgDev.jl](https://github.com/JuliaLang/PkgDev.jl)

PkgDev provides tools for Julia package developers.

[PkgServer.jl](https://github.com/JuliaPackaging/PkgServer.jl)

Reference implementation of a Julia Pkg server, providing advanced package serving and caching capabilities.

[BinaryProvider.jl](https://github.com/JuliaPackaging/BinaryProvider.jl)

A reliable binary provider for Julia.

[Scratch.jl](https://github.com/JuliaPackaging/Scratch.jl)

This repository implements the scratch spaces API for package-specific mutable containers of data. 

[RelocatableFolders.jl](https://github.com/JuliaPackaging/RelocatableFolders.jl)

An alternative to the `@__DIR__` macro. Packages that wish to reference paths in their project directory run into issues with relocatability when used in conjunction with PackageCompiler. The `@path` macro provided by this package overcomes this limitation.

[WinRPM.jl](https://github.com/JuliaPackaging/WinRPM.jl)

WinRPM is an installer for RPM packages provided by an RPM-md build system.

[LazyArtifacts.jl](https://github.com/JuliaPackaging/LazyArtifacts.jl)

This is a wrapper package meant to bridge the gap for packages that want to use the LazyArtifacts stdlib as a dependency within packages that still support Julia versions older than 1.6.

[Artifacts.jl](https://github.com/JuliaPackaging/Artifacts.jl)

This is a wrapper package meant to bridge the gap for packages that want to use the Artifacts as a dependency within packages that still support Julia versions older than 1.6.

[CMake.jl](https://github.com/JuliaPackaging/CMake.jl)

This package is designed to make it easier for Julia packages to build binary dependencies that use CMake. 

[CMakeWrapper.jl](https://github.com/JuliaPackaging/CMakeWrapper.jl)

This package provides a `BinDeps.jl`-compatible CMakeProcess class for automatically building CMake dependencies.

[BinDeps.jl](https://github.com/JuliaPackaging/BinDeps.jl)

Easily build binary dependencies for Julia packages.

[MetadataTools.jl](https://github.com/JuliaPackaging/MetadataTools.jl)

Functionality to analyze the structure of Julia's METADATA repository.

[CondaBinDeps.jl](https://github.com/JuliaPackaging/CondaBinDeps.jl)

This package, which builds on the Conda.jl package allows one to use conda as a BinDeps binary provider for Julia. CondaBinDeps.jl is a cross-platform alternative. It can also be used without administrator rights, in contrast to the current Linux-based providers.

[CMakeBuilder](https://github.com/JuliaPackaging/CMakeBuilder)

This repository builds compiled binaries of cmake for the Julia packages that require this program to build.

[VersionCheck.jl](https://github.com/GenieFramework/VersionCheck.jl)

Utility package for checking if a new version of a Julia package is available. It uses the current `Project.toml` file and a special `CHANGELOG.html` file to determine the latest versions.

[autodocs.jl](https://github.com/Clemapfel/autodocs.jl)

Provides the `@autodocs` macro, which, if invoked at the start of a module declaration, will generate basic documentation for all declared Julia objects (functions, structs, macros, etc.) in that module. If an object already has documentation, it will be expanded (but not overwritten).

---

### Julia programming

[Requires.jl](https://github.com/JuliaPackaging/Requires.jl)

Requires is a Julia package that will magically make loading packages faster, maybe.

[Archspec.jl](https://juliapackaging.github.io/Archspec.jl/dev/)

Archspec is a Julia package to get access to the information provided by `archspec-json`, part of the Archspec project.

[OutputCollectors.jl](https://github.com/JuliaPackaging/OutputCollectors.jl)

This package lets you capture subprocess `stdout` and `stderr` streams independently, resynthesizing and colorizing the streams appropriately.

[Profile.jl](https://github.com/timholy/ProfileView.jl)

This package contains tools for visualizing and interacting with profiling data collected with Julia's built-in sampling profiler.

[FileIO.jl](https://juliaio.github.io/FileIO.jl/stable/)

FileIO aims to provide a common framework for detecting file formats and dispatching to appropriate readers/writers.

[PackageCompiler.jl](https://julialang.github.io/PackageCompiler.jl/dev/index.html)

Julia is, in general, a "just-barely-ahead-of-time" compiled language. When you call a function for the first time, Julia compiles it for precisely the types of the arguments given. This can take some time. All subsequent calls within that same session use this fast compiled function, but if you restart Julia you lose all the compiled work. PackageCompiler allows you to do this work upfront — further ahead of time — and store the results for a lower latency startup.

[Pluto.jl](https://github.com/fonsp/Pluto.jl)

Writing a notebook is not just about writing the final document — Pluto empowers the experiments and discoveries that are essential to getting there.

[PlutoUI.jl](https://juliahub.com/docs/PlutoUI/)

A tiny package to make html `"<input>"` a bit more Julian. Use it with the `@bind` macro in Pluto.

[IJulia.jl](https://julialang.github.io/IJulia.jl/stable/)

IJulia is a Julia-language backend combined with the Jupyter interactive environment (also used by IPython).

[Interact.jl](https://juliagizmos.github.io/Interact.jl/latest/)

This package is a collection of web-based widgets. It works in Jupyter notebooks, Atom IDE, or as a plain old web page.

[Revise.jl](https://timholy.github.io/Revise.jl/stable/)

Revise.jl may help you keep your Julia sessions running longer, reducing the need to restart when you make changes to code.

[Documenter.jl](https://juliadocs.github.io/Documenter.jl/stable/)

A documentation generator for Julia. A package for building documentation from docstrings and markdown files.

[DocumenterMarkdown.jl](https://github.com/JuliaDocs/DocumenterMarkdown.jl)

This package enables the Markdown / MkDocs backend of Documenter.jl.

[CUDA.jl](https://cuda.juliagpu.org/stable/)

The CUDA.jl package is the main entrypoint for programming NVIDIA GPUs in Julia. The package makes it possible to do so at various abstraction levels, from easy-to-use arrays down to hand-written kernels using low-level CUDA APIs.

[AMDGPU.jl](https://amdgpu.juliagpu.org/stable/)

AMD GPU (ROCm) programming in Julia.

[Spark.jl](https://dfdx.github.io/Spark.jl/dev/)

Spark.jl provides an interface to Apache Spark™ platform, including SQL / DataFrame and Structured Streaming. It closely follows the PySpark API, making it easy to translate existing Python code to Julia.

[Threads⨉.jl](https://tkf.github.io/ThreadsX.jl/dev/)

Parallelized Base functions.

[Git.jl](https://github.com/JuliaVersionControl/Git.jl)

Git.jl allows you to use command-line Git in your Julia packages. You do not need to have Git installed on your computer, and neither do the users of your packages!

[JET.jl](https://aviatesk.github.io/JET.jl/stable/)

An experimental code analyzer for Julia, no need for additional type annotations. JET.jl employs Julia's type inference to detect potential bugs.

[Registrator.jl](https://github.com/JuliaComputing/Registrator.jl)

Registrator is a GitHub app that automates creation of registration pull requests for your julia packages to the General registry.

[LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl)

Create and maintain local registries for Julia packages. This package is intended to provide a simple workflow for maintaining local registries (private or public) without making any assumptions about how the registry or the packages are hosted.

[LoopVectorization.jl](https://juliasimd.github.io/LoopVectorization.jl/latest/)

This library provides the `@turbo` macro, which may be used to prefix a for loop or broadcast statement. It then tries to vectorize the loop to improve runtime performance.

[FLoops.jl](https://juliafolds.github.io/FLoops.jl/dev/)

FLoops.jl provides a macro `@floop`. It can be used to generate a fast generic sequential and parallel iteration over complex collections.

[FoldsCUDA.jl](https://juliafolds.github.io/FoldsCUDA.jl/dev/)

FoldsCUDA.jl provides Transducers.jl-compatible fold (reduce) implemented using CUDA.jl. This brings the transducers and reducing function combinators implemented in Transducers.jl to GPU. Furthermore, using FLoops.jl, you can write parallel for loops that run on GPU.

[Pipe.jl](https://github.com/oxinabox/Pipe.jl)

Place `@pipe` at the start of the line for which you want "advanced piping functionality" to work. This works the same as Julia piping, except if you place a underscore in the right-hand expression, it will be replaced with the result of the left-hand expression.

[NetworkOptions.jl](https://github.com/JuliaLang/NetworkOptions.jl)

The `NetworkOptions` package acts as a mediator between ways of configuring network transport mechanisms (SSL/TLS, SSH, proxies, etc.) and Julia packages that provide access to transport mechanisms. This allows the a common interface to configuring things like TLS and SSH host verification and proxies via environment variables (currently) and other configuration mechanisms (in the future), while packages that need to configure these mechanisms can simply ask `NetworkOptions` what to do in specific situations without worrying about how that configuration is expressed.

[Tokenize.jl](https://github.com/JuliaLang/Tokenize.jl)

Tokenize is a Julia package that serves a similar purpose and API as the tokenize module in Python but for Julia.

[JuliaSyntax.jl](https://github.com/JuliaLang/JuliaSyntax.jl)

A Julia frontend, written in Julia.

[BinaryBuilder.jl](https://docs.binarybuilder.org/stable/)

The purpose of the BinaryBuilder.jl Julia package is to provide a system for compiling 3rd-party binary dependencies that should work anywhere the official Julia distribution does.

[JLLPrefixes.jl](https://github.com/JuliaPackaging/JLLPrefixes.jl)

Collect, symlink and copy around prefixes of JLL packages! This package makes it easy to use prefixes of JLL packages outside of Julia, either by symlinking or copying them to a stable prefix.

[Gen.jl](https://www.gen.dev/stable/)

A general-purpose probabilistic programming system with programmable inference, embedded in Julia

[Soss.jl](https://cscherrer.github.io/Soss.jl/stable/)

Soss is a library for probabilistic programming.

[Tilde.jl]ttps://github.com/cscherrer/Tilde.jl)

WIP, successor to Soss.jl.

[AutoSysimages.jl](https://petvana.github.io/AutoSysimages.jl/stable/)

This package automates building of user-specific system images (sysimages) for the specific project.

[NBInclude.jl](https://github.com/stevengj/NBInclude.jl)

NBInclude is a package for the Julia language which allows you to include and execute IJulia (Julia-language Jupyter) notebook files just as you would include an ordinary Julia file.

[SoftGlobalScope.jl](https://github.com/stevengj/SoftGlobalScope.jl)

SoftGlobalScope is a package for the Julia language that simplifies the variable scoping rules for code in global scope. It is intended for interactive shells (the REPL, IJulia, etcetera) to make it easier to work interactively with Julia, especially for beginners.

[ChangePrecision.jl](https://github.com/stevengj/ChangePrecision.jl)

This package makes it easy to change the "default" precision of a large body of Julia code, simply by prefixing it with the `@changeprecision T expression` macro.

[OhMyREPL.jl](https://github.com/KristofferC/OhMyREPL.jl)

A package that hooks into the Julia REPL and gives it syntax highlighting, bracket highlighting, rainbow brackets and other goodies.

[TimerOutputs.jl](https://github.com/KristofferC/TimerOutputs.jl)

TimerOutputs is a small Julia package that is used to generate formatted output from timings made in different sections of a program. It's main functionality is the `@timeit` macro, similar to the `@time` macro in Base except one also assigns a label to the code section being timed. Multiple calls to code sections with the same label (and in the same "scope") will accumulate the data for that label. After the program has executed, it is possible to print a nicely formatted table presenting how much time, allocations and number of calls were made in each section. The output can be customized as to only show the things you are interested in.

[ArgParse.jl](https://github.com/carlobaldassi/ArgParse.jl)

ArgParse.jl is a package for parsing command-line arguments to Julia programs.

[IonCLI.jl](https://github.com/Roger-luo/IonCLI.jl)

A CLI package manager for Julia.

[IonBase.jl](https://github.com/Roger-luo/IonBase.jl)

The Base package for IonCLI. This includes all the builtin functionalities in IonCLI. It can be extended by developers by overloading some of the interfaces.

[RegistryCompatTools.jl](https://github.com/KristofferC/RegistryCompatTools.jl)

Collecting packages you have commit access to.

[Rewrite.jl](https://github.com/HarrisonGrodin/Rewrite.jl)

Rewrite.jl is an efficient symbolic term rewriting engine.

[RewriteRepl.jl](https://github.com/MasonProtter/RewriteRepl.jl)

This package provides a repl mode for the Julia term rewriting system Rewrite.jl.

[Currier.jl](https://github.com/MasonProtter/Currier.jl)

Julia's multiple dispatch offers a lot of expressive power which is a strict superset of things like currying in Haskell. However, sometimes it's convenient to have currying and one doesn't want to write out the methods themselves. 

[SpecializeVarargs.jl](https://github.com/MasonProtter/SpecializeVarargs.jl)

SpecializeVarargs.jl does one thing: force to julia to create and specialize on a given number of varadic arguments. This is likely only useful to people doing very complicated codegen in high performance situations, e.g. in Cassette overdub methods like those used in ForwardDiff2.jl.

[ExprTools.jl](https://github.com/invenia/ExprTools.jl)

ExprTools provides tooling for working with Julia expressions during metaprogramming. This package aims to provide light-weight performant tooling without requiring additional package dependencies.

[MacroTools.jl](https://github.com/FluxML/MacroTools.jl)

MacroTools provides a library of tools for working with Julia code and expressions. This includes a powerful template-matching system and code-walking tools that let you do deep transformations of code in a few lines.

[InfixFunctions.jl](https://github.com/MasonProtter/InfixFunctions.jl)

Julia infix function hack, based on this Python hack: http://code.activestate.com/recipes/384122-infix-operators

[Spec.jl](https://github.com/MasonProtter/Spec.jl)

Spec.jl is an experimental package trying to incorportate ideas from Clojure's spec. The idea in Spec.jl is that we define `@pre_spec` and/or `@post_spec` functions which are run before and/or after a given function when in a validation context.

[BootlegCassette.jl](https://github.com/MasonProtter/BootlegCassette.jl)

BootlegCassette.jl is a quick and dirty package that tries to mimic the interface of Cassette.jl using IRTools.jl under the hood. This isn't a great implementation, but provided you do not use tagging and only use `@context`, `ovderdub`, `prehook`, `posthook` and `recurse`, BootlegCassette.jl should work as a drop-in replacement for Cassette.jl.

[LegibleLambdas.jl](https://github.com/MasonProtter/LegibleLambdas.jl)

Legible Lambdas for Julia.

[ToggleableAsserts.jl](https://github.com/MasonProtter/ToggleableAsserts.jl)

Assertions that can be turned on or off with a switch, with no runtime penalty when they're off.

[NaturallyUnitful.jl](https://github.com/MasonProtter/NaturallyUnitful.jl)

This package reexports Unitful.jl alongside two extra functions: 1) `natural`, a function for converting a given quantity to the Physicist's so-called "natural units", 2) `unnatural`, a function for converting from natural units to a given unnatural unit such as meters.

[StaticModules.jl](https://github.com/MasonProtter/StaticModules.jl)

A StaticModule is basically a little, statically sized and typed namespace you can use for enclosing Julia code and variables without runtime overhead and useable in either the global or local scopes. StaticModules are not a replacement modules, but may be complementary.

[ReplMaker.jl](https://github.com/MasonProtter/ReplMaker.jl)

The idea behind ReplMaker.jl is to make a tool for building (domain specific) languages in Julia.

[Gaius.jl](https://github.com/MasonProtter/Gaius.jl)

Gaius.jl is a multi-threaded BLAS-like library using a divide-and-conquer strategy to parallelism, and built on top of the fantastic LoopVectorization.jl. Gaius spawns threads using Julia's depth first parallel task runtime and so Gaius's routines may be fearlessly nested inside multi-threaded Julia programs.

[CompTime.jl](https://github.com/MasonProtter/CompTime.jl)

The goal of this library is to allow for a simplified style of writing `@generated` functions, inspired by zig comptime features.

[Bumper.jl](https://github.com/MasonProtter/Bumper.jl)

Bumper.jl is an experimental package that aims to make working with bump allocators easy and safer (when used right). You can dynamically allocate memory to these bump allocators, and reset them at the end of a code block, just like Julia’s default stack. Allocating to the a `AllocBuffer` with Bumper.jl can be just as efficient as stack allocation.

[Rhea.jl](https://github.com/jkrumbiegel/Rhea.jl)

A Julia port of [Rhea](https://github.com/Nocte-/rhea), an improved C++ implementation of the Cassowary constraint solver.

[FilteredGroupbyMacro.jl](https://github.com/jkrumbiegel/FilteredGroupbyMacro.jl)

FilteredGroupbyMacro.jl offers a macro `@by` with which a concise syntax for filtered split-apply-combine operations can be expressed concisely. It is very similar in nature to the `[i,j,by]` indexing that the well-known package data.table in the R ecosystem uses.

[ClearStacktrace.jl](https://github.com/jkrumbiegel/ClearStacktrace.jl)

ClearStacktrace.jl is an experimental package that replaces `Base.show_backtrace` with a clearer version that uses alignment and colors to reduce visual clutter and indicate module boundaries, and expands base paths so they are clickable.

[Observables2.jl](https://github.com/jkrumbiegel/Observables2.jl)

A different take on the Observables idea, where listeners and inputs are linked both ways to avoid observable hell.

[SimpleWeave.jl](https://github.com/jkrumbiegel/SimpleWeave.jl)

This package offers a simplified syntax to create documents for use with Weave.jl.

[VersionBenchmarks.jl](https://github.com/jkrumbiegel/VersionBenchmarks.jl)

A package to run benchmarks of different versions, branches, or commits of packages against each other on multiple Julia versions.

[Chain.jl](https://github.com/jkrumbiegel/Chain.jl)

A Julia package for piping a value through a series of transformation expressions using a more convenient syntax than Julia's native piping functionality.

[ReadableRegex.jl](https://github.com/jkrumbiegel/ReadableRegex.jl)

A package for people who don't want to learn or read regexes. ReadableRegex.jl gives you a syntax that is much easier to write and understand than the rather cryptic standard Regex. The syntax is as close as possible to a natural language description of the Regex. Especially for nested and grouped expressions with special characters it is orders of magnitudes easier to understand than a simple regex string. You can also compile the function expression before using it with the @compile macro for lower runtime costs.

[HotTest.jl](https://github.com/jkrumbiegel/HotTest.jl)

HotTest.jl is an experimental package which works in conjunction with Julia's default testing pipeline in Pkg.jl. The tests are run in a sandbox module in the current session, this means that compilation delays only matter for the first run, but each run is still independent from session state. Rerunning the tests afterwards should be quick.

---

### Interoperability

[PyCall.jl](https://github.com/JuliaPy/PyCall.jl)

This package provides the ability to directly call and fully interoperate with Python from the Julia language. You can import arbitrary Python modules from Julia, call Python functions (with automatic conversion of types between Julia and Python), define Python classes from Julia methods, and share large data structures between Julia and Python without copying them.

[Conda.jl](https://github.com/JuliaPy/Conda.jl)

This package allows one to use conda as a cross-platform binary provider for Julia for other Julia packages, especially to install binaries that have complicated dependencies like Python.

[SymPy.jl](https://github.com/JuliaPy/SymPy.jl)

SymPy is a Python library for symbolic mathematics.

[SciPy.jl](https://atsushisakai.github.io/SciPy.jl/stable/)

A Julia interface for SciPy using PyCall.jl.

[PyPlot.jl](https://github.com/JuliaPy/PyPlot.jl)

This module provides a Julia interface to the Matplotlib plotting library from Python, and specifically to the `matplotlib.pyplot` module.

[PyCallJLD.jl](https://github.com/JuliaPy/PyCallJLD.jl)

PyCallJLD enables saving and loading PyCall's PyObjects using JLD.jl.

[Seaborn.jl](https://github.com/JuliaPy/Seaborn.jl)

A Julia wrapper around the Seaborn data visualization library.

[Pandas.jl](https://github.com/JuliaPy/Pandas.jl)

This package provides a Julia interface to the excellent Pandas package. It sticks closely to the Pandas API. One exception is that integer-based indexing is automatically converted from Python's 0-based indexing to Julia's 1-based indexing.

[RCall.jl](https://juliainterop.github.io/RCall.jl/stable/)

Allows the user to call R packages from within Julia.

[MATLAB.jl](https://github.com/JuliaInterop/MATLAB.jl)

The MATLAB.jl package provides an interface for using MATLAB® from Julia using the MATLAB C api.

[Cxx.jl](https://juliainterop.github.io/Cxx.jl/stable/)

Cxx.jl is a Julia package that provides a C++ interoperability interface for Julia. It also provides an experimental C++ REPL mode for the Julia REPL.

[jluna](https://github.com/Clemapfel/jluna)

Julia <-> C++ Wrapper with Focus on Safety, Elegance, and Ease of Use.

---

### IO

[HDF5.jl](https://juliaio.github.io/HDF5.jl/stable/)

HDF5 is a file format and library for storing and accessing data, commonly used for scientific data. HDF5 files can be created and read by numerous programming languages. This package provides an interface to the HDF5 library for the Julia language.

---

### Data

[CSV.jl](https://csv.juliadata.org/stable/)

CSV.jl is a pure-Julia package for handling delimited text data, be it comma-delimited (csv), tab-delimited (tsv), or otherwise.

[DataFrames.jl](https://dataframes.juliadata.org/stable/)

DataFrames.jl provides a set of tools for working with tabular data in Julia.

[DataFramesMeta.jl](https://juliadata.github.io/DataFramesMeta.jl/stable/)

Metaprogramming tools for DataFrames.jl objects to provide more convenient syntax.

[DataFrameMacros.jl](https://github.com/jkrumbiegel/DataFrameMacros.jl)

DataFrames.jl has a special mini-language for data transformations, which is powerful but often verbose.

[Tables.jl](https://tables.juliadata.org/stable/)

This guide provides documentation around the powerful tables interfaces in the Tables.jl package.

[TableMetadataTools.jl](https://github.com/JuliaData/TableMetadataTools.jl)

Tools for working with metadata of Tables.jl tables in Julia.

[WeakRefStrings.jl](https://github.com/JuliaData/WeakRefStrings.jl)

A string type for minimizing data-transfer costs in Julia.

[DataCubes.jl](https://github.com/c-s/DataCubes.jl)

DataCubes provides a framework to handle multidimensional tables.

[PrettyTables.jl](https://ronisbr.github.io/PrettyTables.jl/stable/)

This package has the purpose to print data in matrices in a human-readable format. It was inspired in the functionality provided by ASCII Table Generator.

[Parsers.jl](https://github.com/JuliaData/Parsers.jl)

A collection of type parsers and utilities for Julia.

[TypedTables.jl](https://github.com/JuliaData/TypedTables.jl)

Simple, fast, column-based storage for data analysis in Julia.

[YAML.jl](https://github.com/JuliaData/YAML.jl)

YAML is a flexible data serialization format that is designed to be easily read and written by human beings. 
This library parses YAML documents into native Julia types and dumps them back into YAML documents.

[JLD2.jl](https://juliaio.github.io/JLD2.jl/dev/)

JLD2 saves and loads Julia data structures in a format comprising a subset of HDF5, without any dependency on the HDF5 C library.

[Query.jl](https://www.queryverse.org/Query.jl/stable/)

Query is a package for querying Julia data sources. It can filter, project, join, sort and group data from any iterable data source.

[RData.jl](https://github.com/JuliaData/RData.jl)

Read R data files (.rda, .RData) and optionally convert the contents into Julia equivalents.

[JSONTables.jl](https://github.com/JuliaData/JSONTables.jl)

A package that provides a JSON integration with the Tables.jl interface, that is, it provides the `jsontable` function as a way to treat a JSON object of arrays, or a JSON array of objects, as a Tables.jl-compatible source.

[StructTypes.jl](https://github.com/JuliaData/StructTypes.jl)

Package providing the `StructTypes.StructType` trait for Julia types to declare the kind of "struct" they are, providing serialization/deserialization packages patterns and strategies to automatically construct objects.

[DataAPI.jl](https://github.com/JuliaData/DataAPI.jl)

This package provides a namespace for data-related generic function definitions to solve the optional dependency problem; packages wishing to share and/or extend functions can avoid depending directly on each other by moving the function definition to DataAPI.jl and each package taking a dependency on it.

[Strapping.jl](https://juliadata.github.io/Strapping.jl/stable/)

"Strapping" stands for STruct Relational MAPPING, and provides ORM-like functionality for Julia, including:

[TableOperations.jl](https://github.com/JuliaData/TableOperations.jl)

Common table operations on Tables.jl compatible sources.

[SplitApplyCombine.jl](https://github.com/JuliaData/SplitApplyCombine.jl)

Strategies for nested data in Julia.

[MemPool.jl](https://github.com/JuliaData/MemPool.jl)

Simple distributed datastore that supports custom serialization, spilling least recently used data to disk and memory-mapping.

[IndexedTables.jl](https://github.com/JuliaData/IndexedTables.jl)

IndexedTables provides tabular data structures where some of the columns form a sorted index.

[FlatBuffers.jl](https://flatbuffers.juliadata.org/stable/)

FlatBuffers.jl provides native Julia support for reading and writing binary structures following the google flatbuffer schema.

[Missings.jl](https://github.com/JuliaData/Missings.jl)

Convenience functions for working with missing values in Julia.

[Avro.jl](https://juliadata.github.io/Avro.jl/stable/)

The Avro.jl package provides a pure Julia implementation for reading writing data in the avro format.

[Feather.jl](https://github.com/JuliaData/Feather.jl)

Julia library for working with feather-formatted files.

[JuliaDB.jl](https://juliadb.juliadata.org/stable/)

JuliaDB is a package for working with persistent data sets.

[JuliaDBMeta.jl](https://github.com/JuliaData/JuliaDBMeta.jl)

JuliaDBMeta is a set of macros to simplify data manipulation with JuliaDB, heavily inspired on DataFramesMeta.

[DBFTables.jl](https://github.com/JuliaData/DBFTables.jl)

Read xBase / dBASE III+ .dbf files in Julia.

[Arrow.jl](https://arrow.juliadata.org/dev/)

A pure Julia implementation of the apache arrow memory format specification.

[Dataverse.jl](https://gdcc.github.io/Dataverse.jl/dev/)

This package is about interfaces to the Dataverse project APIs, collections, datasets, etc.

[JSON3.jl](https://quinnj.github.io/JSON3.jl/stable/)

Yet another JSON package for Julia; this one is for speed and slick struct mapping.

[Chemfiles.jl](https://github.com/chemfiles/Chemfiles.jl)

This package contains the Julia binding for the chemfiles library. It allow you, as a programmer, to read and write chemistry trajectory files easily, with the same simple interface for all the supported formats. For more information, please read the introduction to chemfiles.

[SumTypes.jl](https://github.com/MasonProtter/SumTypes.jl)

A julian implementation of sum types. Sum types, sometimes called 'tagged unions' are the type system equivalent of the disjoint union operation (which is not a union in the traditional sense). From a category theory perspective, sum types are interesting because they are dual to Tuples (whatever that means).

[MutableNamedTuples.jl](https://github.com/MasonProtter/MutableNamedTuples.jl)

Sometimes you want a named tuple, but mutable.

---

#### Arrays

[PooledArrays.jl](https://github.com/JuliaData/PooledArrays.jl)

A pooled representation of arrays for purposes of compression when there are few unique elements.

[CategoricalArrays.jl](https://categoricalarrays.juliadata.org/stable/)

The package provides the `CategoricalArray` type designed to hold categorical data (either unordered/nominal or ordered/ordinal) efficiently and conveniently.

[InvertedIndices.jl](https://github.com/JuliaData/InvertedIndices.jl)

This very small package just exports one type: the `InvertedIndex`, or `Not` for short. It can wrap any supported index type and may be used as an index into any `AbstractArray` subtype, including OffsetArrays.

[AxisArrays.jl](https://github.com/JuliaArrays/AxisArrays.jl)

This package for the Julia language provides an array type (the AxisArray) that knows about its dimension names and axis values. 

[ArrayInterface.jl](https://juliaarrays.github.io/ArrayInterface.jl/stable/)

Designs for new Base array interface primitives, used widely through scientific machine learning (SciML) and other organizations

[OffsetArrays.jl](https://juliaarrays.github.io/OffsetArrays.jl/stable/)

OffsetArrays provides Julia users with arrays that have arbitrary indices, similar to those found in some other programming languages like Fortran.

[StructsOfArrays.jl](https://github.com/JuliaArrays/StructsOfArrays.jl)

StructsOfArrays implements the classic structure of arrays optimization. The contents of a given field for all objects is stored linearly in memory, and different fields are stored in different arrays. This permits SIMD optimizations in more cases and can also save a bit of memory if the object contains padding. It is especially useful for arrays of complex numbers.

[IdentityRanges.jl](https://github.com/JuliaArrays/IdentityRanges.jl)

IdentityRanges are Julia-language a helper type for creating "views" of arrays.

[CatIndices.jl](https://github.com/JuliaArrays/CatIndices.jl)

A Julia package for concatenating, growing, and shrinking arrays in ways that allow control over the resulting axes.

[IndirectArrays.jl](https://github.com/JuliaArrays/IndirectArrays.jl)

An `IndirectArray` is one that encodes data using a combination of an `index` and a `value` table. Each element is assigned its own index, which is used to retrieve the value from the `value` table.  Concretely, if `A` is an `IndirectArray`, then `A[i,j...] = value[index[i,j,...]]`.

[MappedArrays.jl](https://github.com/JuliaArrays/MappedArrays.jl)

This package implements "lazy" in-place elementwise transformations of arrays for the Julia programming language.

[UnsafeArrays.jl](https://github.com/JuliaArrays/UnsafeArrays.jl)

UnsafeArrays provides stack-allocated pointer-based array views for Julia.

[CustomUnitRanges.jl](https://github.com/JuliaArrays/CustomUnitRanges.jl)

This Julia package supports the creation of array types with "unconventional" indices, i.e., when the indices may not start at 1.

[AxisArrays.jl](https://github.com/JuliaArrays/AxisArrays.jl)

This package for the Julia language provides an array type (the `AxisArray`) that knows about its dimension names and axis values. This allows for indexing by name without incurring any runtime overhead. This permits one to implement algorithms that are oblivious to the storage order of the underlying arrays. AxisArrays can also be indexed by the values along their axes, allowing column names or interval selections.

[ArraysOfArrays.jl](https://juliaarrays.github.io/ArraysOfArrays.jl/stable/)

A Julia package for efficient storage and handling of nested arrays. ArraysOfArrays provides two different types of nested arrays: `ArrayOfSimilarArrays` and `VectorOfArrays`.

[ElasticArrays.jl](https://github.com/JuliaArrays/ElasticArrays.jl)

ElasticArrays provides resizeable multidimensional arrays for Julia.

[TiledIteration.jl](https://github.com/JuliaArrays/TiledIteration.jl)

This Julia package handles some of the low-level details for writing cache-efficient, possibly-multithreaded code for multidimensional arrays.

[ShiftedArrays.jl](https://juliaarrays.github.io/ShiftedArrays.jl/stable/)

A `ShiftedArray` is a lazy view of an Array, shifted on some or all of his indexing dimensions by some constant values.

[StaticArrays.jl](https://juliaarrays.github.io/StaticArrays.jl/stable/)

StaticArrays provides a framework for implementing statically sized arrays in Julia, using the abstract type `StaticArray{Size,T,N} <: AbstractArray{T,N}`. 

[MetadataArrays.jl](https://github.com/JuliaArrays/MetadataArrays.jl)

Implementation of arrays with metadata.

[LazyArrays.jl](https://juliaarrays.github.io/LazyArrays.jl/dev/)

Lazy arrays and linear algebra in Julia

[BlockArrays.jl](https://juliaarrays.github.io/BlockArrays.jl/stable/)

A block array is a partition of an array into multiple blocks or subarrays, see wikipedia for a more extensive description.

[StructArrays.jl](https://juliaarrays.github.io/StructArrays.jl/stable/)

This package introduces the type `StructArray` which is an `AbstractArray` whose elements are struct (for example `NamedTuples`, or `ComplexF64`, or a custom user defined struct).

[EndpointRanges.jl](https://github.com/JuliaArrays/EndpointRanges.jl)

This Julia package makes it easier to index "unconventional" arrays (ones for which indexing does not necessarily start at 1), by defining constants `ibegin` and `iend` that stand for the beginning and end, respectively, of the indices range along any given dimension.

[InfiniteArrays.jl](https://github.com/JuliaArrays/InfiniteArrays.jl)

A Julia package for representing arrays with infinite dimension sizes, designed to work with other array types. Infinite arrays are by necessity lazy, and so this package is closely linked to `LazyArrays.jl`.

[FillArrays.jl](https://github.com/JuliaArrays/FillArrays.jl)

Julia package to lazily represent matrices filled with a single entry, as well as identity matrices. This package exports the following types: `Eye`, `Fill`, `Ones`, `Zeros`, `Trues` and `Falses`.

[LazyGrids.jl](https://juliaarrays.github.io/LazyGrids.jl/stable/)

This Julia module exports a method ndgrid for generating lazy versions of grids from a collection of 1D vectors (any `AbstractVector` type).

[HybridArrays.jl](https://github.com/JuliaArrays/HybridArrays.jl)

Arrays with both statically and dynamically sized axes in Julia.

[SentinelArrays.jl](https://github.com/JuliaData/SentinelArrays.jl)

Array types that can use sentinel values of the element type for special values.

---

#### Strings

[InlineStrings.jl](https://github.com/JuliaStrings/InlineStrings.jl)

Fixed-width string types for facilitating certain string workflows in Julia.

[StringEncodings.jl](https://github.com/JuliaStrings/StringEncodings.jl)

This Julia package provides support for decoding and encoding texts between multiple character encodings.

[StringViews.jl](https://github.com/JuliaStrings/StringViews.jl)

This Julia package implements a new type of `AbstractString`, a `StringView`, that provides a string representation of any underlying array of bytes (any `AbstractVector{UInt8}`), interpreted as UTF-8 encoded Unicode data.

[NaturalSort.jl](https://github.com/JuliaStrings/NaturalSort.jl)

Natural Sort Order in Julia.

[TinySegmenter.jl](https://github.com/JuliaStrings/TinySegmenter.jl)

TinySegmenter.jl is a Julia version of TinySegmenter, which is an extremely compact Japanese tokenizer originally written in JavaScript by Mr. Taku Kudo.

[WeakRefStrings.jl](https://github.com/JuliaData/WeakRefStrings.jl)

A string type for minimizing data-transfer costs in Julia.

---

#### Geospatial

[GMT.jl](https://github.com/GenericMappingTools/GMT.jl)

GMT is a toolbox for Earth, Ocean, and Planetary Science. Besides its own map projections implementation and mapping capabilities it wraps the GDAL library which gives access to most of of raster and vector formats used in geospatial world.

[Geodesy.jl](https://github.com/JuliaGeo/Geodesy.jl)

Geodesy is a Julia package for working with points in various world and local coordinate systems.

[ASCIIrasters.jl](https://github.com/JuliaGeo/ASCIIrasters.jl)

Simple read and write functions for ASCII raster files.

[Proj.jl](https://github.com/JuliaGeo/Proj.jl)

A simple Julia wrapper around the PROJ cartographic projections library.

[GDAL.jl](https://github.com/JuliaGeo/GDAL.jl)

Julia wrapper for GDAL - Geospatial Data Abstraction Library. This package is a binding to the C API of GDAL/OGR. It provides only a C style usage, where resources must be closed manually, and datasets are pointers.

[GeoJSON.jl](https://juliageo.org/GeoJSON.jl/stable/)

Read GeoJSON files using JSON3.jl, and provide the Tables.jl interface.

[GeoParquet.jl](https://github.com/JuliaGeo/GeoParquet.jl)

Adding `geospatial` data to Parquet. Follows the GeoParquet v0.4 spec.

[GeoInterface.jl](https://juliageo.org/GeoInterface.jl/stable/)

An interface for geospatial vector data in Julia.

[LibGEOS.jl](https://github.com/JuliaGeo/LibGEOS.jl)

LibGEOS is a package for manipulation and analysis of planar geometric objects, based on the libraries GEOS (the engine of PostGIS) and JTS (from which GEOS is ported).

[GeoDatasets.jl](https://juliageo.org/GeoDatasets.jl/stable/)

The aim of this package is to give access to common geographics datasets.

[DimensionalArrayTraits.jl](https://github.com/JuliaGeo/DimensionalArrayTraits.jl)

Abstract base package for dimensional arrays and their specific traits.

[Shapefile.jl](https://github.com/JuliaGeo/Shapefile.jl)

This library supports reading ESRI Shapefiles in pure Julia.

[GADM.jl](https://github.com/JuliaGeo/GADM.jl)

This package provides polygons/multipolygons for all countries and their sub-divisions from the GADM dataset. It fetches the data dynamically from the officially hosted database using DataDeps.jl and provides a minimal wrapper API to get the coordinates of the requested geometries.

[CFTime.jl](https://juliageo.org/CFTime.jl/stable/)

`CFTime` encodes and decodes time units conforming to the Climate and Forecasting (CF) netCDF conventions. 

[Leaflet.jl](https://juliageo.org/Leaflet.jl/stable/)

LeafletJS maps for Julia.

[GeoFormatTypes.jl](https://juliageo.org/GeoFormatTypes.jl/stable/)

GeoFormatTypes defines wrapper types to make it easy to pass and dispatch on geographic formats like Well Known Text or GeoJSON between packages. 

[LibSpatialIndex.jl](https://github.com/JuliaGeo/LibSpatialIndex.jl)

LibSpatialIndex.jl is a julia wrapper around the C API of libspatialindex, for spatially indexing kD bounding box data.

[NetCDF.jl](https://juliageo.org/NetCDF.jl/dev/)

Reading and writing NetCDF files in Julia.

---

### Data analysis

[Impute.jl](https://invenia.github.io/Impute.jl/latest/)

Impute.jl provides various methods for handling missing data in Vectors, Matrices and Tables.

[LazyGrids.jl](https://juliaarrays.github.io/LazyGrids.jl/stable/)

This Julia module exports a method ndgrid for generating lazy versions of grids from a collection of 1D vectors (any AbstractVector type).

[Interpolations.jl](https://juliamath.github.io/Interpolations.jl/latest/)

This package implements a variety of interpolation schemes for the Julia language. It has the goals of ease-of-use, broad algorithmic support, and exceptional performance.

[ScatteredInterpolation.jl](https://eljungsk.github.io/ScatteredInterpolation.jl/stable/)

Interpolation of scattered data in Julia.

[GridInterpolations.jl](https://github.com/sisl/GridInterpolations.jl)

This package performs multivariate interpolation on a rectilinear grid. At the moment, it provides implementations of multilinear and simplex interpolation.

[Fuzzy.jl](https://github.com/phelipe/Fuzzy.jl)

Mamdani and Sugeno type Fuzzy Inference System in Julia.

[FFTViews.jl](https://github.com/JuliaArrays/FFTViews.jl)

A package for simplifying operations that involve Fourier transforms. An FFTView of an array uses periodic boundary conditions for indexing, and shifts all indices of the array downward by 1.

[PaddedViews.jl](https://github.com/JuliaArrays/PaddedViews.jl)

PaddedViews provides a simple wrapper type, `PaddedView`, to add "virtual" padding to any array without copying data. Edge values not specified by the array are assigned a `fillvalue`. Multiple arrays may be "promoted" to have common indices using the `paddedviews` function.

[SpatioTemporalTraits.jl](https://github.com/JuliaArrays/SpatioTemporalTraits.jl)

SpatioTemporalTraits serves as a relatively low-level source of spatiotemporal traits, allowing other packages the opportunity to use a common interface for their unique types.

---

### Graphics

#### Graphics

[Javis.jl](https://juliaanimators.github.io/Javis.jl/stable/)

Javis.jl is a tool focused on providing an easy to use interface for making animations and developing visualizations quickly - while having fun!

[Luxor.jl](https://juliagraphics.github.io/Luxor.jl/stable/)

Luxor is a Julia package for drawing simple static vector graphics. It provides basic drawing functions and utilities for working with shapes, polygons, clipping masks, PNG and SVG images, turtle graphics, and simple animations.

[Layered.jl](https://github.com/jkrumbiegel/Layered.jl)

A library for creating 2D vector graphics in layers. Check out the documentation!

[Animations.jl](https://github.com/jkrumbiegel/Animations.jl)

Animations.jl offers an easy way to set up simple animations where multiple keyframes are interpolated between in sequence. You can choose different easing functions or create your own. Keyframe values can be anything that can be linearly interpolated, you can also add your own methods for special types. An easing can have repetitions and delays, so that looping animations are simpler to create.

---

#### Plots

[Plots.jl](https://docs.juliaplots.org/latest/)

Plots - powerful convenience for visualization in Julia.

[ColorSchemes.jl](https://juliagraphics.github.io/ColorSchemes.jl/stable/)

This package provides a collection of colorschemes.

[PlotThemes.jl](https://github.com/JuliaPlots/PlotThemes.jl)

PlotThemes is a package to spice up the plots made with Plots.jl.

[StatsPlots.jl](https://github.com/JuliaPlots/StatsPlots.jl)

This package is a drop-in replacement for Plots.jl that contains many statistical recipes for concepts and types introduced in the JuliaStats organization.

[UnicodePlots.jl](https://github.com/JuliaPlots/UnicodePlots.jl)

Advanced Unicode plotting library designed for use in Julia's REPL.

[Makie.jl](https://makie.juliaplots.org/stable/documentation/)

Makie is a data visualization ecosystem for the Julia programming language, with high performance and extensibility.

[MakieThemes.jl](https://github.com/JuliaPlots/MakieThemes.jl)

The idea of this package is to create a collection of themes for Makie to customize the size and look of plot elements and colors.

[MakieTeX.jl](https://github.com/JuliaPlots/MakieTeX.jl)

MakieTeX allows you to draw and visualize arbitrary TeX documents in Makie! You can insert anything from a single line of math to a large and complex TikZ diagram.

[RPRMakie.jl](https://makie.juliaplots.org/stable/documentation/backends/rprmakie/)

Experimental ray tracing backend using AMDs RadeonProRender. While it's created by AMD and tailored to Radeon GPUs, it still works just as well for NVidia and Intel GPUs using OpenCL. It also works on the CPU and even has a hybrid modus to use GPUs and CPUs in tandem to render images.

[Biplots.jl](https://github.com/MakieOrg/Biplots.jl)

Biplot recipes in 2D and 3D for Makie.jl.

[TopoPlots.jl](https://makieorg.github.io/TopoPlots.jl/stable/)

A package for creating topoplots from data that were measured on arbitrarily positioned sensors.

[GraphMakie.jl](https://graph.makie.org/stable/)

This Package consists of two parts: a plot recipe for graphs types from Graphs.jl and some helper functions to add interactions to those plots.

[OSMMakie.jl](https://github.com/MakieOrg/OSMMakie.jl)

A Makie recipe for plotting OpenStreetMap data. It makes heavy use of the GraphMakie package and extends it to work with the specific features of an OSMGraph.

[GeoMakie.jl](https://geo.makie.org/stable/)

GeoMakie.jl is a Julia package for plotting geospatial data on a given map projection.

[GMT.jl](https://github.com/GenericMappingTools/GMT.jl)

GMT.jl is a collection of tools for manipulating geographic and Cartesian data sets (including filtering, trend fitting, gridding, projecting, etc.) and producing vector graphics illustrations ranging from simple x-y plots via contour maps to artificially illuminated surfaces and 3D perspective views.

[AlgebraOfGraphics.jl](https://juliaplots.org/AlgebraOfGraphics.jl/stable/)

AlgebraOfGraphics defines a language for data visualization. It is based on a few simple building blocks that can be combined using `+` and `*`.

[Winston.jl](https://winston.readthedocs.io/en/latest/#)

Winston: 2D Plotting for Julia.

[Gaston.jl](https://mbaz.github.io/Gaston.jl/stable/)

Gaston is a Julia package for plotting. It provides an interface to gnuplot, a mature, powerful, and actively developed plotting package available on all major platforms.

[PlotlyJS.jl](https://juliaplots.org/PlotlyJS.jl/stable/)

This package does not interact with the Plotly web API, but rather leverages the underlying javascript library to construct plotly graphics using all local resources. This means you do not need a Plotly account or an internet connection to use this package.

[Gnuplot.jl](https://docs.juliahub.com/Gnuplot/4mcTU/1.3.0/)

The Gnuplot.jl package allows easy and fast use of gnuplot as a data visualization tool in Julia.

[VennEuler.jl](https://github.com/JuliaPlots/VennEuler.jl)

Generate area-proportional Venn/Euler diagrams in Julia. This is based, in part, on algorithms from Leland Wilkinson.

[PairPlots.jl](https://github.com/sefffal/PairPlots.jl)

This package produces corner plots, otherwise known as pair plots or scatter plot matrices: grids of 1D and 2D histograms that allow you to visualize high dimensional data.

[Gadfly.jl](https://gadflyjl.org/stable/)

Gadfly is a system for plotting and visualization written in Julia. It is based largely on Hadley Wickhams's ggplot2 for R and Leland Wilkinson's book The Grammar of Graphics.

[Compose.jl](https://giovineitalia.github.io/Compose.jl/latest/)

Compose is a declarative vector graphics system written in Julia. It's designed to simplify the creation of complex graphics and serves as the basis of the Gadfly data visualization package.

[PythonPlot.jl](https://github.com/stevengj/PythonPlot.jl)

This module provides a Julia interface to the Matplotlib plotting library from Python, and specifically to the `matplotlib.pyplot` module.

[CMPlot.jl](https://github.com/g-insana/CMPlot.jl)

Cloudy Mountain Plot in Julia. An informative RDI categorical distribution plot inspired by Violin, Bean and Pirate Plots. (RDI = Raw data + Descriptive statistics + Inferential statistics)

[Isoband.jl](https://github.com/jkrumbiegel/Isoband.jl)

Isoband.jl wraps isoband_jll, which gives access to wilkelab's isoband package, which powers contour plots in ggplot.

---

#### Graphs

[Graphs.jl](https://juliagraphs.org/Graphs.jl/dev/)

The goal of Graphs.jl is to offer a performant platform for network and graph analysis in Julia, following the example of libraries such as NetworkX in Python.

[GraphRecipes](https://docs.juliaplots.org/stable/GraphRecipes/introduction/)

GraphRecipes is a collection of recipes for visualizing graphs. Users specify a graph through an adjacency matrix, an adjacency list, or an AbstractGraph via Graphs. GraphRecipes will then use a layout algorithm to produce a visualization of the graph that the user passed.

[NetworkLayout.jl](https://juliagraphs.org/NetworkLayout.jl/stable/)

Layout algorithms for graphs and trees in pure Julia.

[SimpleWeightedGraphs.jl](https://github.com/JuliaGraphs/SimpleWeightedGraphs.jl)

Edge-Weighted Graphs for Graphs.jl.

[MatrixNetworks.jl](https://github.com/JuliaGraphs/MatrixNetworks.jl)

This package consists of a collection of network algorithms. In short, the major difference between MatrixNetworks.jl and packages like LightGraphs.jl or Graphs.jl is the way graphs are treated.

[MultilayerGraphs.jl](https://juliagraphs.org/MultilayerGraphs.jl/stable/)

MultilayerGraphs.jl is a Julia package for the construction, manipulation and analysis of multilayer graphs extending Graphs.jl.

[GraphPlot.jl](https://github.com/JuliaGraphs/GraphPlot.jl)

Graph layout and visualization algorithms based on Compose.jl and inspired by GraphLayout.jl.

[MetaGraphs.jl](https://juliagraphs.org/MetaGraphs.jl/dev/)

MetaGraphs.jl graphs with arbitrary metadata.

[GraphIO.jl](https://github.com/JuliaGraphs/GraphIO.jl)

GraphIO provides support to Graphs.jl for reading/writing graphs in various formats.

[StaticGraphs.jl](https://github.com/JuliaGraphs/StaticGraphs.jl)

Memory-efficient, performant graph structures optimized for large networks.

[GraphViz.jl](https://github.com/JuliaGraphs/GraphViz.jl)

This package provides an interface to the the GraphViz package for graph visualization.

[GraphsFlows.jl](https://juliagraphs.org/GraphsFlows.jl/dev/)

Flow algorithms on top of Graphs.jl, including maximum_flow, multiroute_flow and mincost_flow.

[GraphDataFrameBridge.jl](https://github.com/JuliaGraphs/GraphDataFrameBridge.jl)

Tools for interoperability between DataFrame objects and LightGraphs and MetaGraphs objects.

[CommunityDetection.jl](https://github.com/JuliaGraphs/CommunityDetection.jl)

Implements community detection for Graphs.jl. Both Nonbacktracking and Bethe Hessian detection are supported.

[GraphsMatching.jl](https://github.com/JuliaGraphs/GraphsMatching.jl)

Matching algorithms on top of Graphs.jl.

[GraphsExtras.jl](https://github.com/JuliaGraphs/GraphsExtras.jl)

Extra functionality for Graphs.jl.

[LightGraphs.jl](https://github.com/sbromberger/LightGraphs.jl)

LightGraphs offers both (a) a set of simple, concrete graph implementations -- Graph (for undirected graphs) and DiGraph (for directed graphs), and (b) an API for the development of more sophisticated graph implementations under the AbstractGraph type.

[LightGraphsExtras.jl](https://github.com/JuliaGraphs/LightGraphsExtras.jl)

Extra functionality for Graphs.

[VegaGraphs.jl](https://github.com/JuliaGraphs/VegaGraphs.jl)

VegaGraphs implements graph visualization with Vega-Lite.

[SpecialGraphs.jl](https://github.com/JuliaGraphs/SpecialGraphs.jl)

Encoding special graph structures in types.

---

#### TeX

[LaTeXStrings.jl](https://github.com/stevengj/LaTeXStrings.jl)

This is a small package to make it easier to type LaTeX equations in string literals in the Julia language, written by Steven G. Johnson.

[MathTeXEngine.jl](https://github.com/Kolaru/MathTeXEngine.jl)

This is a work in progress package aimed at providing a pure Julia engine for LaTeX math mode. It is composed of two main parts: a LaTeX parser and a LaTeX engine, both only for LaTeX math mode.

[TikzPictures.jl](https://github.com/JuliaTeX/TikzPictures.jl)

This library allows one to create Tikz pictures and save in various formats. It integrates with IJulia, outputting SVG images to the notebook.

[TreeView.jl](https://github.com/JuliaTeX/TreeView.jl)

This is a small package to visualize a graph corresponding to an abstract syntax tree (AST) of a Julia expression. It uses the TikzGraphs.jl package to do the visualization.

[PGFPlots.jl](https://github.com/JuliaTeX/PGFPlots.jl)

This library uses the LaTeX package pgfplots to produce plots. It integrates with IJulia, outputting SVG images to the notebook.

[TikzGraphs.jl](https://github.com/JuliaTeX/TikzGraphs.jl)

This library generates graph layouts using the TikZ graph layout package.

[TikzCDs.jl](https://github.com/JuliaTeX/TikzCDs.jl)

A wrapper around TikzPictures.jl for easier drawing of commutative diagrams using `tikz-cd`.

[BibTeX.jl](https://github.com/JuliaTeX/BibTeX.jl)

Parsing BibTeX files for the Julia language.

---

### Mathematics

[JuMP.jl](https://jump.dev/JuMP.jl/stable/)

JuMP is a domain-specific modeling language for mathematical optimization embedded in Julia.

[Calculus.jl](https://github.com/JuliaMath/Calculus.jl)

The Calculus package provides tools for working with the basic calculus operations of differentiation and integration. You can use the Calculus package to produce approximate derivatives by several forms of finite differencing or to produce exact derivative using symbolic differentiation. You can also compute definite integrals by different numerical methods.

[QuadGK.jl](https://juliamath.github.io/QuadGK.jl/stable/)

This package provides support for one-dimensional numerical integration in Julia using adaptive Gauss-Kronrod quadrature.

[Combinatorics.jl](https://juliamath.github.io/Combinatorics.jl/dev/)

A combinatorics library for Julia, focusing mostly (as of now) on enumerative combinatorics and permutations. As overflows are expected even for low values, most of the functions always return BigInt, and are marked as such below.

[DataStructures.jl](https://juliacollections.github.io/DataStructures.jl/latest/)

This package implements a variety of data structures.

[SpecialMatrices.jl](https://github.com/JuliaMatrices/SpecialMatrices.jl)

This Julia package extends the LinearAlgebra library with support for special matrices that are used in linear algebra.

[ToeplitzMatrices.jl](https://github.com/JuliaMatrices/ToeplitzMatrices.jl)

Fast matrix multiplication and division for Toeplitz, Hankel and circulant matrices in Julia.

[BlockSparseMatrices.jl](https://github.com/KristofferC/BlockSparseMatrices.jl)

Blocked Sparse Matrices in Julia.

[SpecialMatrices.jl](https://github.com/JuliaMatrices/SpecialMatrices.jl)

A Julia package for working with special matrix types.

[Nemo.jl](https://nemocas.github.io/Nemo.jl/latest/)

Nemo is a computer algebra package for the Julia programming language.

[FastTransforms.jl](https://juliaapproximation.github.io/FastTransforms.jl/stable/)

FastTransforms.jl allows the user to conveniently work with orthogonal polynomials with degrees well into the millions.

[NaNMath.jl](https://github.com/mlubin/NaNMath.jl)

Implementations of basic math functions which return NaN instead of throwing a DomainError.

[Polynomials.jl](https://juliamath.github.io/Polynomials.jl/stable/)

Polynomials.jl is a Julia package that provides basic arithmetic, integration, differentiation, evaluation, and root finding for univariate polynomials.

[NFFT.jl](https://juliamath.github.io/NFFT.jl/dev/)

Julia package for the Non-equidistant Fast Fourier Transform

[NFFT3.jl](https://nfft.github.io/NFFT3.jl/stable/)

The nonequispaced fast Fourier transform or NFFT, see [Keiner, Kunis, Potts, 2006] and [Plonka, Potts, Steidl, Tasche, 2018], overcomes one of the main shortcomings of the FFT - the need for an equispaced sampling grid.

[Measurements.jl](https://juliaphysics.github.io/Measurements.jl/stable/)

Measurements.jl relieves you from the hassle of propagating uncertainties coming from physical measurements, when performing mathematical operations involving them. The linear error propagation theory is employed to propagate the errors.

[SingularSpectrumAnalysis.jl](https://github.com/baggepinnen/SingularSpectrumAnalysis.jl)

A package for performing [Singular Spectrum Analysis (SSA)](https://en.wikipedia.org/wiki/Singular_spectrum_analysis).

[Symbolics.jl](https://symbolics.juliasymbolics.org/stable/)

Symbolics.jl is a fast and modern Computer Algebra System (CAS) for a fast and modern programming language (Julia). The goal is to have a high-performance and parallelized symbolic algebra system that is directly extendable in the same language as the users.

[DifferentialEquations.jl](https://github.com/SciML/DifferentialEquations.jl)

This is a suite for numerically solving differential equations written in Julia and available for use in Julia, Python, and R.

[HCubature.jl](https://github.com/JuliaMath/HCubature.jl)

The HCubature module is a pure-Julia implementation of multidimensional "h-adaptive" integration. That is, given an n-dimensional integral

[NLsolve.jl](https://github.com/JuliaNLSolvers/NLsolve.jl)

Solving non-linear systems of equations in Julia.

[Roots.jl](https://docs.juliahub.com/Roots/o0Xsi/2.0.0/)

Roots is a Julia package for finding zeros of continuous scalar functions of a single real variable.

[RowEchelon.jl](https://github.com/blegat/RowEchelon.jl)

This small package contains the functions `rref` and `rref!`.

[NumericExtensions.jl](https://github.com/lindahua/NumericExtensions.jl)

Julia extensions to provide high performance computational support.

[HyperCubicRoots.jl](https://github.com/jd-foster/HyperCubicRoots.jl)

Julia implementation of the real cubic root finding method described in: [https://www.jstor.org/stable/27821778](https://www.jstor.org/stable/27821778).

[RealPolynomialRoots.jl](https://github.com/jverzani/RealPolynomialRoots.jl)

A package to find isolating intervals for the real roots of a square free polynomial.

[RootSystems.jl](https://github.com/tkluck/RootSystems.jl)

A simple library that contains implementations of the irreducible root systems.

[RootsUFPB.jl](https://github.com/eduardovegas/RootsUFPB.jl)

Package that implements some root finding methods in Julia.

[RootsByDistribution.jl](https://github.com/cbilz/RootsByDistribution.jl)

A Julia package for finding roots of a continuous function when the approximate distribution of the roots is known.

[RootSolvers.jl](https://clima.github.io/RootSolvers.jl/dev/)

A simple GPU-capable root solver package.

[SimpleRoots.jl](https://rafaqz.github.io/SimpleRoots.jl/stable/)

A light-weight collection of simple root-finding algorithms.

[RootsAndPoles.jl](https://github.com/fgasdia/RootsAndPoles.jl)

Global complex Roots and Poles Finding in Julia.

[FastPolynomialRoots.jl](https://github.com/andreasnoack/FastPolynomialRoots.jl)

This package is a Julia wrapper of the Fortran programs accompanying Fast and Backward Stable Computation of Roots of Polynomials by Jared L. Aurentz, Thomas Mach, Raf Vandebril and David S. Watkins.

[RationalRoots.jl](https://github.com/Jutho/RationalRoots.jl)

This package provides a data type `RationalRoot{T<:Integer}` to exactly represent the (positive or negative) square root of a rational number of type `Rational{T}`.

[PolynomialRoots.jl](https://github.com/giordano/PolynomialRoots.jl)

PolynomialRoots.jl is a library for finding roots of complex univariate polynomials, written in Julia.

[GeometryPrimitives.jl](https://github.com/stevengj/GeometryPrimitives.jl)

This package provides a set of geometric primitive types (balls, cuboids, cylinders, and so on) and operations on them designed to enable piecewise definition of functions, especially for finite-difference and finite-element simulations, in the Julia language.

[FastChebInterp.jl](https://github.com/stevengj/FastChebInterp.jl)

Fast multidimensional Chebyshev interpolation on a hypercube (Cartesian-product) domain, using a separable (tensor-product) grid of Chebyshev interpolation points, as well as Chebyshev regression (least-square fits) from an arbitrary set of points. In both cases we support arbitrary dimensionality, complex and vector-valued functions, and fast derivative and Jacobian computation.

[Sobol.jl](https://github.com/stevengj/Sobol.jl)

This module provides a free Julia-language Sobol low-discrepancy-sequence (LDS) implementation. This generates "quasi-random" sequences of points in N dimensions which are equally distributed over an N-dimensional hypercube.

[Simpson.jl](https://codeberg.org/AdamWysokinski/Simpson.jl)

Simpson.jl is a Julia package to integrate y(x) using samples and the composite Simpson's rule.

[Ferrite.jl](https://ferrite-fem.github.io/Ferrite.jl/stable/)

Ferrite is a finite element toolbox that provides functionalities to implement finite element analysis in Julia. The aim is to be general and to keep mathematical abstractions.

[CALFEM.jl](https://github.com/KristofferC/CALFEM.jl)

CALFEM.jl is an API port of the simple Matlab FE toolbox CALFEM written in Julia. The purpose of this package is to ease the transition for people who want to try out Julia for FE-analysis. CALFEM.jl is built on top of Ferrite.

[Completion.jl](https://github.com/HarrisonGrodin/Completion.jl)

Knuth-Bendix completion algorithm.

[SpecialSets.jl](https://github.com/HarrisonGrodin/SpecialSets.jl)

SpecialSets provides implementations of sets commonly used in mathematics, as well as the logic for cleanly combining such sets.

[Simplify.jl](https://github.com/HarrisonGrodin/Simplify.jl)

Simplify.jl implements methods for symbolic algebraic simplification in the Julia language.

[Conjugates.jl](https://github.com/MasonProtter/Conjugates.jl)

Conjugates.jl is a simple little utility for doing algebraic operations between an object and its ‘conjugate’, either Hermitian conjugate or transpose.

[MatrixProductStates.jl](https://github.com/MasonProtter/MatrixProductStates.jl)

This is a package-in-progress in which I am implementing the DMRG algorithm over matrix product states as explained in Schollwöck’s The density-matrix renormalization group in the age of matrix product states. A similar project has been undertaken in LatticeSweeper.jl.

[LatticeSweeper.jl](https://github.com/0/LatticeSweeper.jl)

Simple two-site DMRG. No support for quantum number conservation.

[Symbolics.jl](https://github.com/MasonProtter/Symbolics.jl)

This is a package I'm throwing together after getting inspired by the talk Physics in Clojure which was about porting scmutils to clojure. scmutils is a Scheme package with a very interesting and powerful computer algebra system meant as a companion to the book Structure and Interpretation of Classical Mechanics.

[SymbolicTracing.jl](https://github.com/MasonProtter/SymbolicTracing.jl)

Experimental tracing for SymbolicUtils.jl types. SymbolicTracing.jl will allow you to effectively treat `Symbolic{T}` as being a subtype of `T` for the purposes of dispatch if `T` is an abstract type.

[GeometricMatrixAlgebras.jl](https://github.com/MasonProtter/GeometricMatrixAlgebras.jl)

This package is a playground for learning about Geometric Algebra (GA), and intends to leverage Julia's powerful Linear Algebra ecosystem to compute quantities in GA using matrices as a backend.

---

### Artificial Intelligence

[MLJ.jl](https://alan-turing-institute.github.io/MLJ.jl/dev/)

A Machine Learning Framework for Julia.

[ScientificTypes.jl](https://juliaai.github.io/ScientificTypes.jl/stable/)

This package makes a distinction between machine type and scientific type of a Julia object.

[Flux.jl](https://fluxml.ai/Flux.jl/stable/)

Flux is a library for machine learning geared towards high-performance production pipelines.

[Knet.jl](https://denizyuret.github.io/Knet.jl/stable/)

Knet (pronounced "kay-net") is the Koç University deep learning framework implemented in Julia by Deniz Yuret and collaborators.

[FastAI.jl](https://fluxml.ai/FastAI.jl/dev/README.md.html)

FastAI.jl is inspired by fastai, and is a repository of best practices for deep learning in Julia.

[ModelingToolkit.jl](https://mtk.sciml.ai/stable/)

ModelingToolkit.jl is a modeling language for high-performance symbolic-numeric computation in scientific computing and scientific machine learning.

[GlobalSensitivity.jl](https://gsa.sciml.ai/stable/)

Global Sensitivity Analysis (GSA) methods are used to quantify the uncertainty in output of a model with respect to the parameters.

[DecisionTrees.jl](https://github.com/kafisatz/DecisionTrees.jl)

A pure Julia implementation of Decision Tree Algorithms for Regression.

[NearestNeighbors.jl](https://github.com/KristofferC/NearestNeighbors.jl)

NearestNeighbors.jl is a package written in Julia to perform high performance nearest neighbor searches in arbitrarily high dimensions.

[Lathe.jl](https://github.com/ChifiSource/Lathe.jl)

Lathe.jl is an all-in-one package for predictive modeling in Julia. It comes packaged with a Stats Library, Preprocessing Tools, Distributions, Machine-Learning Models, and Model Validation. Lathe features easy object-oriented programming methodologies using Julia's dispatch.

---

### Signal analysis

[AbstractFFTs.jl](https://juliamath.github.io/AbstractFFTs.jl/stable/)

This package is mainly not intended to be used directly. Instead, developers of packages that implement FFTs (such as FFTW.jl or FastTransforms.jl) extend the types/functions defined in AbstractFFTs.

[FFTW.jl](https://juliamath.github.io/FFTW.jl/stable/fft/)

This package extends the functionality provided by AbstractFFTs

[DSP.jl](https://docs.juliadsp.org/stable/contents/)

DSP.jl provides a number of common digital signal processing routines in Julia.

[Deconvolution.jl](https://juliadsp.org/Deconvolution.jl/dev/)

Deconvolution.jl provides a set of functions to deconvolve digital signals, like images or time series.

[Wavelets.jl](https://github.com/JuliaDSP/Wavelets.jl)

A Julia package for fast wavelet transforms (1-D, 2-D, 3-D, by filtering or lifting). The package includes discrete wavelet transforms, column-wise discrete wavelet transforms, and wavelet packet transforms.

[ContinuousWavelets.jl](https://github.com/UCD4IDS/ContinuousWavelets.jl)

This package is an offshoot of Wavelets.jl for the continuous wavelets.

[WaveletsExt.jl](https://ucd4ids.github.io/WaveletsExt.jl/stable/)

This package is a Julia extension package to Wavelets.jl (WaveletsExt is short for Wavelets Extension).

[Estimation.jl](https://github.com/JuliaDSP/Estimation.jl)

A julialang package for DSP related estimation.

[MFCC.jl](https://github.com/JuliaDSP/MFCC.jl)

A package to compute Mel Frequency Cepstral Coefficients.

[FindPeaks1D](https://ymtoo.github.io/FindPeaks1D.jl/stable/)

Finding peaks in a 1-D signal in Julia. The implementation is based on find_peaks in SciPy.

[CubicSplines.jl](https://github.com/sp94/CubicSplines.jl)

A simple package for interpolating 1D data with Akima cubic splines, based on "A New Method of Interpolation and Smooth Curve Fitting Based on Local Parameters", Akima, 1970.

[EntropyHub.jl](https://github.com/MattWillFlood/EntropyHub.jl)

EntropyHub: An open-source toolkit for entropic time series analysis.

[MDCT.jl](https://github.com/stevengj/MDCT.jl)

This module computes the modified discrete cosine transform (MDCT) in the Julia language and the inverse transform (IMDCT), using the fast type-IV discrete cosine tranform (DCT-IV) functions in the FFTW.jl package.

[WorldVocoder.jl](https://github.com/jkrumbiegel/WorldVocoder.jl)

WorldVocoder.jl is a wrapper package for the WORLD vocoder by mmorise https://github.com/mmorise/World. It calls the original WORLD binary via the WORLD_jll.jl package.

---

### Statistics

[StatsKit.jl](https://github.com/JuliaStats/StatsKit.jl)

This is a convenience meta-package which allows loading essential packages for statistics.

[Bootstrap.jl](https://github.com/juliangehring/Bootstrap.jl)

Bootstrap.jl: Statistical Bootstrapping

[CategoricalArrays.jl](https://categoricalarrays.juliadata.org/stable/)

The package provides the CategoricalArray type designed to hold categorical data (either unordered/nominal or ordered/ordinal) efficiently and conveniently.

[Clustering.jl](https://juliastats.org/Clustering.jl/stable/)

Clustering.jl is a Julia package for data clustering.

[Distances.jl](https://github.com/JuliaStats/Distances.jl)

A Julia package for evaluating distances(metrics) between vectors.

[HypothesisTests.jl](https://juliastats.org/HypothesisTests.jl/stable/)

This package implements several hypothesis tests in Julia.

[KernelDensity.jl](https://github.com/JuliaStats/KernelDensity.jl)

Kernel density estimators for Julia.

[Loess.jl](https://github.com/JuliaStats/Loess.jl)

This is a pure Julia loess implementation, based on the fast kd-tree based approximation, implemented in the netlib loess C/Fortran code, and used by many, including in R's loess function.

[MultivariateStats.jl](https://juliastats.org/MultivariateStats.jl/dev/)

A Julia package for multivariate statistics and data analysis (e.g. dimensionality reduction).

[MixedModels.jl](https://juliastats.org/MixedModels.jl/stable/)

MixedModels.jl is a Julia package providing capabilities for fitting and examining linear and generalized linear mixed-effect models.

[StatsBase.jl](https://juliastats.org/StatsBase.jl/stable/)

StatsBase.jl is a Julia package that provides basic support for statistics. Particularly, it implements a variety of statistics-related functions, such as scalar statistics, high-order moment computation, counting, ranking, covariances, sampling, and empirical density estimation.

[StatsModels.jl](https://juliastats.org/StatsModels.jl/stable/)

This package provides common abstractions and utilities for specifying, fitting, and evaluating statistical models.

[StatsFuns.jl](https://github.com/JuliaStats/StatsFuns.jl)

Mathematical functions related to statistics.

[GLM.jl](https://juliastats.org/GLM.jl/stable/)

Linear and generalized linear models in Julia.

[Distributions.jl](https://juliastats.org/Distributions.jl/latest/)

The Distributions package provides a large collection of probabilistic distributions and related functions.

[TimeSeries.jl](https://juliastats.org/TimeSeries.jl/dev/)

The TimeSeries package provides convenient methods for working with time series data in Julia.

[MultivariateStats.jl](https://multivariatestatsjl.readthedocs.io/en/stable/index.html#)

MultivariateStats.jl is a Julia package for multivariate statistical analysis. It provides a rich set of useful analysis techniques, such as PCA, CCA, LDA, PLS, etc.

[Discreet.jl](https://github.com/cynddl/Discreet.jl)

Discreet is a small opinionated toolbox to estimate entropy and mutual information from discrete samples. It contains methods to adjust results and correct over- or under-estimations.

[InformationMeasures.jl](https://github.com/Tchanders/InformationMeasures.jl)

Entropy and mutual information.

[Lasso.jl](https://juliastats.org/Lasso.jl/stable/)

Lasso.jl is a pure Julia implementation of the glmnet coordinate descent algorithm for fitting linear and generalized linear Lasso and Elastic Net models.

[MultipleTesting.jl](https://juliangehring.github.io/MultipleTesting.jl/stable/)

The MultipleTesting package offers common algorithms for p-value adjustment and combination as well as the estimation of the proportion π₀ of true null hypotheses.

[ConjugatePriors.jl](https://github.com/JuliaStats/ConjugatePriors.jl)

A Julia package to support conjugate prior distributions.

[PGM.jl](https://github.com/JuliaStats/PGM.jl)

PGM.jl is a Julia framework for probabilistic graphical models.

[QuantEcon.jl](https://quantecon.github.io/QuantEcon.jl/latest/index.html)

QuantEcon.jl is a Julia package for doing quantitative economics.

[FreqTables.jl](https://github.com/nalimilan/FreqTables.jl)

This package allows computing one- or multi-way frequency tables (a.k.a. contingency or pivot tables) from any type of vector or array.

[OnlineStats.jl](https://joshday.github.io/OnlineStats.jl/stable/)

OnlineStats does statistics and data visualization for big/streaming data via online algorithms. Each algorithm: 1) processes data one observation at a time. 2) uses O(1) memory.

[EmpiricalRisks.jl](https://github.com/lindahua/EmpiricalRisks.jl)

This Julia package provides a collection of predictors and loss functions, mainly to support the implementation of (regularized) empirical risk minimization methods.

[Regression.jl](https://github.com/lindahua/Regression.jl)

This package is based on EmpiricalRisks, and provides a set of algorithms to perform regression analysis. This package supports all regression problems that can be formulated as regularized empirical risk minimization.

---

### Image processing

[JuliaImages.jl](https://juliaimages.org/stable/)

JuliaImages is a collection of packages specifically focused on image processing.

[ImageView.jl](https://github.com/JuliaImages/ImageView.jl)

An image display GUI for Julia.

[MosaicViews.jl](https://github.com/JuliaArrays/MosaicViews.jl)

When visualizing images, it is not uncommon to provide a 2D view of different image sources. For example, comparing multiple images of different sizes, getting a preview of machine learning dataset. This package aims to provide easy-to-use tools for such tasks.

[ImageIO.jl](https://github.com/JuliaIO/ImageIO.jl)

FileIO.jl integration for image files.

[ImageTransformations.jl](https://juliaimages.org/ImageTransformations.jl/stable/#ImageTransformations.jl)

This package provides support for image resizing, image rotation, and other spatial transformations of arrays.

[ImageFiltering.jl](https://juliaimages.org/ImageFiltering.jl/stable/)

ImageFiltering supports linear and nonlinear filtering operations on arrays, with an emphasis on the kinds of operations used in image processing.

[ImageBinarization.jl](https://juliaimages.org/ImageBinarization.jl/stable/)

A Julia package containing a number of algorithms for analyzing images and automatically binarizing them into background and foreground.

[HistogramThresholding.jl](https://juliaimages.org/HistogramThresholding.jl/stable/)

A Julia package for analyzing a one-dimensional histogram and automatically choosing a threshold which partitions the histogram into two parts.

[ImageContrastAdjustment.jl](https://juliaimages.org/ImageContrastAdjustment.jl/stable/)

A Julia package for enhancing and manipulating image contrast.

[ImageDistances.jl](https://github.com/JuliaImages/ImageDistances.jl)

ImageDistances.jl aims to: follow the same API in Distances.jl, support image types, provide image-specific distances.

[ImageSegmentation.jl](https://juliaimages.org/stable/pkgs/segmentation/)

Image Segmentation is the process of partitioning the image into regions that have similar attributes.

[ImageInpainting.jl](https://github.com/JuliaImages/ImageInpainting.jl)

Image inpainting algorithms in Julia.

[ImageQualityIndexes.jl](https://github.com/JuliaImages/ImageQualityIndexes.jl)

ImageQualityIndexes provides the basic image quality assessment methods. Check the reasoning behind the code design here if you're interested in.

[ImageFeatures.jl](https://juliaimages.org/ImageFeatures.jl/stable/)

ImageFeatures is a package for identifying and characterizing "keypoints" (salient features) in images.

[ImageMorphology.jl](https://juliaimages.org/ImageMorphology.jl/stable/)

This package provides morphology operations for structure analysis and image processing.

[ImageMetadata.jl](https://github.com/JuliaImages/ImageMetadata.jl)

ImageMetadata is a simple package providing utilities for working with images that have metadata attached.

[ImageDraw.jl](https://juliaimages.org/ImageDraw.jl/stable/)

A drawing package for JuliaImages.

[JpegTurbo.jl](https://github.com/stevengj/JpegTurbo.jl)

This is a work-in-progress Julia package to provide Julia-callable wrappers to the libjpeg-turbo C library.

---

### Neuroscience

[EDFPlus.jl](https://github.com/wherrera10/EDFPlus.jl)

Julia for handling BDF+ and EDF+ EEG and similar signal data files.

[EEG.jl](https://github.com/JuliaPackageMirrors/EEG.jl)

Process EEG files in Julia.

[Psychophysics.jl](https://github.com/sam81/Psychophysics.jl)

Miscellaneous Julia utilities for psychophysics research.

[Synchrony.jl](https://github.com/simonster/Synchrony.jl)

[Neuroimaging.jl](https://rob-luke.github.io/Neuroimaging.jl/dev/)

Process neuroimaging data using the Julia language.

[NIRX.jl](https://github.com/rob-luke/NIRX.jl)

Read NIRX functional near-infrared spectroscopy files in Julia.

[BDF.jl](https://github.com/sam81/BDF.jl)

BDF.jl is a Julia module to read/write BIOSEMI 24-bit BDF files (used for storing electroencephalographic recordings).

[EDF.jl](https://beacon-biosignals.github.io/EDF.jl/stable/)

Read and write European Data Format (EDF/EDF+) and BioSemi Data Format (BDF) files in Julia.

[EDFPlus.jl](https://github.com/wherrera10/EDFPlus.jl)

Julia for handling BDF+ and EDF+ EEG and similar signal data files.

[AuditoryStimuli.jl](https://github.com/rob-luke/AuditoryStimuli.jl)

Generate auditory stimuli for real-time applications. Specifically, stimuli that are used in auditory research.

[NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl)

EEG/MEG/NIRS/NIBS analysis with Julia.

[Unfold.jl](https://github.com/unfoldtoolbox/Unfold.jl)

Toolbox to perform linear regression on biological signals.

[UnfoldMakie.jl](https://github.com/unfoldtoolbox/UnfoldMakie.jl)

UnfoldMakie allows many visualizations for ERP and "Unfolded"-models. Building on the Unfold and Makie, it grants users highly customizable plots.

---

### Web

[GenieFramework.jl](https://github.com/GenieFramework/GenieFramework.jl)

Meta package for Genie reactive apps.

[Genie.jl](https://github.com/GenieFramework/Genie.jl)

Genie is a full-stack web framework that provides a streamlined and efficient workflow for developing modern web applications. It builds on Julia's strengths (high-level, high-performance, dynamic, JIT compiled), exposing a rich API and a powerful toolset for productive web development.

[GenieUI.jl](https://github.com/GenieFramework/GenieUI.jl)

Higher level UI elements for Genie apps.

[GenieAutoreload.jl](https://github.com/GenieFramework/GenieAutoreload.jl)

GenieAutoReload monitors the indicated files and folders (recursively) and automatically recompiles the Julia code and reloads the corresponding browser window.

[GeniePackageManager.jl](https://github.com/GenieFramework/GeniePackageManager.jl)

Package Management solution for Genie.jl and GenieBuilder Apps.

[GeniePlugins.jl](https://github.com/GenieFramework/GeniePlugins.jl)

Genie plugin providing functionality for creating and working with Genie plugins.

[GenieAuthentication.jl](https://github.com/GenieFramework/GenieAuthentication.jl)

Authentication plugin for Genie.jl.

[GenieAuthorisation.jl](https://github.com/GenieFramework/GenieAuthorisation.jl)

Role Based Authorisation (RBA) plugin for Genie.jl.

[GenieCache.jl](https://github.com/GenieFramework/GenieCache.jl)

Abstract package for Genie caching.

[GenieSession.jl](https://github.com/GenieFramework/GenieSession.jl)

Sessions support for Genie apps.

[GenieSessionFileSession.jl](https://github.com/GenieFramework/GenieSessionFileSession.jl)

File system based session adapter for GenieSession.jl.

[GenieCacheFileCache.jl](https://github.com/GenieFramework/GenieCacheFileCache.jl)

File system cache adapter for GenieCache.jl.

[GenieDevTools.jl](https://github.com/GenieFramework/GenieDevTools.jl)

GenieDevTools.

[GenieDeploy.jl](https://github.com/GenieFramework/GenieDeploy.jl)

GenieDeploy.

[GenieDeployHeroku.jl](https://github.com/GenieFramework/GenieDeployHeroku.jl)

GenieDeployHeroku.

[GenieDeployJuliaHub.jl](https://github.com/GenieFramework/GenieDeployJuliaHub.jl)

GenieDeployJuliaHub.

[DockerizedGBApp](https://github.com/GenieFramework/DockerizedGBApp)

Genie on Docker.

[SearchLight.jl](https://github.com/GenieFramework/SearchLight.jl)

SearchLight is the ORM layer of Genie.jl, the high-performance high-productivity Julia web framework.

[SearchLightSerializerJSON.jl](https://github.com/GenieFramework/SearchLightSerializerJSON.jl)

JSON data serializer for SearchLight.

[SearchLightSQLite.jl](https://github.com/GenieFramework/SearchLightSQLite.jl)

SQLite adapter for SearchLight.

[SearchLightPostgreSQL.jl](https://github.com/GenieFramework/SearchLightPostgreSQL.jl)

Postgres adapter for SearchLight.

[SearchLightMySQL.jl](https://github.com/GenieFramework/SearchLightMySQL.jl)

MySQL adapter for SearchLight.

[SearchLightOracle.jl](https://github.com/GenieFramework/SearchLightOracle.jl)

The project fullfills the API of SearchLight. The adpater can be used for communicating with Oracle databases.

[Stipple.jl](https://github.com/GenieFramework/Stipple.jl)

Stipple is a reactive UI library for building interactive data applications in pure Julia. It uses Genie.jl (on the server side) and Vue.js (on the client). Stipple uses a high performance MVVM architecture, which automatically synchronizes the state two-way (server -> client and client -> server) sending only JSON data over the wire. The Stipple package provides the fundamental communication layer, extending Genie's HTML API with a reactive component.

[Stipple.jl](https://github.com/GenieFramework/Stipple.jl)

Stipple is a reactive UI library for building interactive data applications in pure Julia.

[StippleUI.jl](https://github.com/GenieFramework/StippleUI.jl)

StippleUI is a library of reactive UI elements for Stipple.jl.

[StippleCharts.jl](https://github.com/GenieFramework/StippleCharts.jl)

StippleCharts is a library of reactive charts for Stipple.jl.

[StipplePlotly.jl](https://github.com/GenieFramework/StipplePlotly.jl)

Embedding Plotly Charts in Stipple.

[StipplePlotlyExport.jl](https://github.com/GenieFramework/StipplePlotlyExport.jl)

StipplePlotlyExport.

[StippleLatex.jl](https://github.com/GenieFramework/StippleLatex.jl)

StippleLatex.

[SwagUI.jl](https://github.com/GenieFramework/SwagUI.jl)

Want to use Swagger UI in Julia? This package has your back!

[SwaggerMarkdown.jl](https://github.com/GenieFramework/SwaggerMarkdown.jl)

Swagger Markdown allows you to generate swagger.json for API documentation from the julia source code. The package uses marco to process the markdown that contains an API endpoint's documentation. The markdowon needs to follow the paths described by the OpenAPI Specification (v3, v2), and in YAML format.

[Franklin.jl](https://github.com/tlienart/Franklin.jl)

Franklin is a simple static site generator (SSG) oriented towards technical blogging (code, maths, ...), flexibility and extensibility. The base syntax is plain markdown with a few extensions such as the ability to define and use LaTeX-like commands in or outside of maths environments and the possibility to evaluate code blocks on the fly.

[FranklinTemplates.jl](https://github.com/tlienart/FranklinTemplates.jl)

Templates for Franklin, the static-site generator in Julia. Most of these templates are adapted from existing, popular templates with minor modifications to accommodate Franklin's content.

---

### Misc packages

[SeisNoise.jl](https://tclements.github.io/SeisNoise.jl/latest/)

SeisNoise.jl provides routines for quickly and efficiently implementing seismic interferometry.

[Hexagons.jl](https://github.com/GiovineItalia/Hexagons.jl)

This package provides some basic utilities for working with hexagonal grids.

[Processing.jl](https://github.com/JuliaPackageMirrors/Processing.jl)

A port of the Processing language (https://www.processing.org) to Julia.

[GeoStats.jl](https://juliaearth.github.io/GeoStats.jl/v0.14/)

An extensible framework for high-performance geostatistics in Julia.

[Term.jl](https://fedeclaudi.github.io/Term.jl/dev/)

Term.jl is a Julia library for producing styled, beautiful terminal output.

[AnsiColor.jl](https://github.com/Aerlinger/AnsiColor.jl)

Full support for ANSI colored strings in Julia. Allows formatted output in REPL/Shell environment for both Unix and Mac.

[IOIndents.jl](https://github.com/KristofferC/IOIndents.jl)

IOIndents facilitates writing indented and aligned text to buffers (like files or the terminal).

[Crayons.jl](https://github.com/KristofferC/Crayons.jl)

Crayons is a package that makes it simple to write strings in different colors and styles to terminals. It supports the 16 system colors, both the 256 color and 24 bit true color extensions, and the different text styles available to terminals. The package is designed to perform well, have no dependencies and load fast (about 10 ms load time after precompilation).

[Preferences.jl](https://github.com/JuliaPackaging/Preferences.jl)

The Preferences package provides a convenient, integrated way for packages to store configuration switches to persistent TOML files, and use those pieces of information at both run time and compile time in Julia v1.6+.

[Weave.jl](https://weavejl.mpastell.com/stable/)

Weave is a scientific report generator/literate programming tool for Julia. It resembles Pweave, knitr, R Markdown, and Sweave.

[JuliaBerry.jl](https://github.com/JuliaBerry/JuliaBerry.jl/blob/master/REQUIRE)

An omnibus package with a high level API for controlling peripherals on the Raspberry Pi computer. Currently has support for the GPIO pins on the Pi, and the ExplorerHat.

[PiGPIO.jl](https://docs.juliahub.com/PiGPIO/8aGxa/0.2.0/)

Control GPIO pins on the Raspberry Pi from Julia.

[DataStructuresAlgorithms.jl](https://github.com/mgcth/DataStructuresAlgorithms.jl)

Implementation of some data structures and algorithms from the Chalmers courses DAT038 and TIN093 in Julia as en exercise and for fun.

[DataStructures.jl](https://github.com/hesseltuinhof/DataStructures.jl)

This repository only serves an educational purpose. It implements a large number of data structures and algorithms from the similarly named book by Clifford A. Schaffer.

[FilesystemDatastructures.jl](https://github.com/staticfloat/FilesystemDatastructures.jl)

This package collects useful filesystem datastructures. Currently, it implements two file caches: SizeConstrainedFileCache and NFileCache.

[FastPriorityQueues.jl](https://gdalle.github.io/FastPriorityQueues.jl/dev/)

Possibly faster alternatives to the priority queue from DataStructures.jl. See the documentation for details.

[TrackRoots.jl](https://github.com/yakir12/TrackRoots.jl)

This is a Julia script for analysing light intensities as a function of time and space in time-lapse image stacks of seedling roots.

[ConstLab.jl](https://github.com/KristofferC/ConstLab.jl)

ConstLab.jl is a small package for Julia to test and experiment with constitutive models. It's main functionality is to call a user given material routine over a time range, aggregate the material response and return it back for analysis. The load applied to the material can be user generated or one of the predefined load cases can be used. To facilitate visualizing the results, some auxiliary plot functions are provided.

[Inflector.jl](https://github.com/GenieFramework/Inflector.jl)

Utility module for working with grammatical rules (singular, plural, underscores, etc).

---

## Books

### JuliaProgramming

Bakshi T. Tanmay Teaches Julia for Beginners: A Springboard to Machine Learning for All Ages.: 9781260456639

Balbaert I. Getting Started with Julia. ISBN: 9781783284795

Balbaert I. Julia 1.0 Programming - Second Edition. ISBN: 9781788999090

Balbaert I, Salceanu A. Julia 1.0 programming complete reference guide: discover Julia, a high-performance language for technical computing. ISBN: 9781838822248

Dash S. Hands-on Julia Programming. ISBN: 9789391030919

Engheim E. Julia as a Second Language. ISBN: 1617299715

Engheim E. Julia for Beginners.

Joshi A, Lakhanpal R. Learning Julia. ISBN: 9781785883279

Kalicharan N. Julia - Bit by Bit. ISBN: 9783030739362

Kalicharan N. Julia - Bit by Bit: Programming for Beginners (Undergraduate Topics in Computer Science) ISBN: 303073935X

Kamiński B, Szufel P. Julia 1.0 Programming Cookbook. ISBN: 9781788998369

Kerns G. Introduction to Julia.

Kwon C. Julia Programming for Operations Research.

Kwong T. Hands-on design patterns and best practices with Julia: proven solutions to common problems in software design for Julia 1.x. ISBN: 9781838648817

Lauwens B. Think Julia: How to Think Like a Computer Scientist. ISBN: 9781484251898

Lobianco A. Julia Quick Syntax Reference: A Pocket Guide for Data Science Programming. ISBN: 9781484251904

Nagar S. Beginning Julia Programming. ISBN: 9781484231708

Rohit JR. Julia Cookbook: over 40 recipes to get you up and running with programming using Julia. ISBN: 9781785882012

Salceanu A. Julia Programming Projects. ISBN: 9781788292740

Sengupta A, Sherrington M, Balbaert I. Julia: High Performance Programming 2E. ISBN: 9781788298117

Sengupta A, Sherrington M, Balbaert I. Julia: High Performance Programming. ISBN: 9781787125704

Sengupta A. Julia High Performance. ISBN: 9781785880919

Sengupta A. Julia high performance: design and develop high performing programs with Julia. ISBN: 9781785880919

Sengupta A. The Little Book of Julia Algorithms: A workbook to develop fluency in Julia programming. ISBN: 9781838173609

Sherrington M. Mastering Julia: develop your analytical and programming skills further in Julia to solve complex data processing problems. ISBN: 9781783553310

Zea DJ. Interactive Visualization and Plotting with Julia: Create impressive data visualizations through Julia packages. ISBN: 1801810516

---

### Data science

Numerical methods for Scientifc Computing. ISBN: 9798985421804

Adams C. Learning Microeconometrics with R. ISBN: 9780367255381

Boyd S, Vandenberghe S. Introduction to Applied Linear Algebra – Vectors, Matrices, and Least Squares. ISBN: 9781316518960 

Chan S. Introduction to Probability for Data Science. ISBN: 978-1-60785-747-1

Joshi A. Julia for data science: explore the world of data science from scratch with Julia by your side. ISBN: 9781785289699

Kamiński B, Prałat P. Train Your Brain - Challenging Yet Elementary Mathematics. ISBN: 9780367564872

Kamiński B. Julia for Data Analysis. ISBN: 1633439364

Kamiński B. Julia for Data Analysis. ISBN: 9781633439368

Kochenderfer M, Wheeler T, Wray K. Algorithms for Decision Making. ISBN: 9780262047012

Kochenderfer M, Wheeler T. Algorithms for Optimization. ISBN: 9780262039420

McNicholas P, Tait P. Data science with Julia. ISBN: 9781351013673

Orban D, Arioli M. Iterative Solution of Symmetric Quasi-Definite Linear Systems. DOI: 10.1137/1.9781611974737

Storopoli J, Huijzer R, Alonso L. Julia data science. ISBN: 9798489859165

Voulgaris Z. Julia for Data Science.

---

### Statistics

Nazarathy Y, Klok H. Statistics with Julia: Fundamentals for Data Science, Machine Learning and Artificial Intelligence (Springer Series in the Data Sciences). ISBN: 3030709000

---

### Artificial intelligence

Voulgaris Z. Julia for Machine Learning. ISBN: 9781634628136

---

### Signal analysis

---

### Image processing

Cudihins D. Hands-On Computer Vision with Julia. ISBN: 9781788998796

---

## Misc

[Quantitative Economics with Julia](https://julia.quantecon.org/intro.html)

This website presents a set of lectures on quantitative economic modeling, designed and written by Jesse Perla, Thomas J. Sargent and John Stachurski. The language instruction is Julia.

[JuliaNotes.jl](https://m3g.github.io/JuliaNotes.jl/stable/)

A collection of explanations and tips to make the best use of Julia. Many answers are extracts from solutions provided to user questions in the Julia Discourse. 

Novak K. [Numerical Methods for Scientific Computing](https://www.equalsharepress.com/media/NMFSC.pdf) ISBN: 9798985421804

The book covers the mathematical theory and practical considerations of the essential numerical methods used in scientific computing. Julia is used throughout, with Python and Matlab/Octave included in the back matter. Jupyter notebooks of the code are [available on GitHub](https://nbviewer.org/github/nmfsc/julia/blob/main/julia.ipynb).

[Using Julia on the HPC](https://blogs.nottingham.ac.uk/digitalresearch/2022/09/30/using-julia-on-the-hpc/)

---

## Tutorials

[Julia language: a concise tutorial](https://syl1.gitbook.io/julia-language-a-concise-tutorial/)

[Exploratory PCA in Julia](https://stackoverflow.com/questions/68053860/exploratory-pca-in-julia)

[Julia macros for beginners](https://jkrumbiegel.com/pages/2021-06-07-macros-for-beginners/)

[A really brief introduction to audio signal processing in Julia](https://www.seaandsailor.com/audiosp_julia.html)

[Julia: delete rows and columns from an array or matrix](https://stackoverflow.com/questions/58033504/julia-delete-rows-and-columns-from-an-array-or-matix)

Nybo Nissen J. [What scientists must know about hardware to write fast code](https://biojulia.net/post/hardware/)

The aim of this tutorial is to give non-professional programmers a brief overview of the features of modern hardware that you must understand in order to write fast code.

[JuliaBerry](https://juliaberry.github.io/)

JuliaBerry is an organisation that brings together various resources for using the Julia language for the Raspberry Pi.

[Beautiful Makie](https://beautiful.makie.org/dev/)

A gallery of easy to copy-paste examples. 

[Julia Tutorials Template](https://github.com/rikhuijzer/JuliaTutorialsTemplate)

A template for building websites with Julia. Easily publish Pluto notebooks online.

[Julia by Example](https://juliabyexample.helpmanual.io/)